<?php

namespace Drupal\link_tree\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_tree' formatter.
 *
 * @FieldFormatter(
 *   id = "link_tree_unordered",
 *   label = @Translation("Link Tree: unordered list"),
 *   field_types = {
 *     "link_tree"
 *   }
 * )
 */
class LinkTreeUnordered extends LinkFormatter {

  /**
   * The links created by the link formatter.
   *
   * @var array
   */
  protected array $links;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    // Get the original list of links.
    $this->links = parent::viewElements($items, $langcode);

    // Create the field element.
    $element[] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $this->getLinkTree($items),
    ];

    return $element;
  }

  /**
   * Build the link tree based on the link tree form values.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The items of the link tree.
   * @param string $id
   *   The parent ID of one or more link tree items.
   *
   * @return array
   *   The link tree.
   */
  protected function getLinkTree(FieldItemListInterface &$items, string $id = ''): array {
    $tree = [];
    foreach ($items as $key => $item) {
      if ($id === $item->parent) {
        if ($link = $this->getLink($key)) {
          $tree[$item->id]['link'] = $link;
          $tree[$item->id]['children'] = $this->getLinkTree($items, $item->id);
        }
      }
    }

    return $tree;
  }

  /**
   * Gets a link from the available links list.
   *
   * @param int|string $key
   *   The array key for the links list.
   *
   * @return array
   *   The link data array.
   */
  protected function getLink(int|string $key): array {
    if (!(array_key_exists($key, $this->links))) {
      return [];
    }

    return $this->links[$key];
  }

}
