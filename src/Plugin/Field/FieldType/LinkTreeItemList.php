<?php

namespace Drupal\link_tree\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;

/**
 * Represents a configurable entity link tree field.
 */
class LinkTreeItemList extends FieldItemList {

  /**
   * Get the tree.
   *
   * @return array
   *   The list tree.
   */
  public function getTree(): array {
    $list = $this->list;
    return $this->buildTree($list);
  }

  /**
   * Build the link tree.
   *
   * @param array $items
   *   The link items.
   * @param string $parent_id
   *   The parent ID of one or more link items.
   *
   * @return array
   *   The link tree.
   */
  protected function buildTree(array &$items, string $parent_id = ''): array {
    $tree = [];

    foreach ($items as $delta => $item) {
      $id = $item->get('id')->getValue();

      // We definitely need an ID to build the link tree.
      if (!$id) {
        continue;
      }

      // Build the link tree.
      if ($item->get('parent')->getValue() === $parent_id) {
        $children = $this->buildTree($items, $id);
        if ($children) {
          $item->set('has_children', TRUE);
          $item->set('children', $children);
        }
        $tree[$id] = $item;
        unset($items[$delta]);
      }
    }

    return $tree;
  }

}
