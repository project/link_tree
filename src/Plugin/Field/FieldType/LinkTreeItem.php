<?php

namespace Drupal\link_tree\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Plugin implementation of the 'link' field type.
 *
 * @FieldType(
 *   id = "link_tree",
 *   label = @Translation("Link tree"),
 *   description = @Translation("Stores a URL string, optional varchar link text, and optional blob of attributes to assemble a link."),
 *   default_widget = "link_tree_default",
 *   default_formatter = "link",
 *   list_class = "\Drupal\link_tree\Plugin\Field\FieldType\LinkTreeItemList",
 * )
 */
class LinkTreeItem extends LinkItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = parent::propertyDefinitions($field_definition);
    // Add a unique ID to allow referencing the parent link.
    $properties['id'] = DataDefinition::create('string')
      ->setLabel(t('ID'));
    // Add a property for the parent link.
    $properties['parent'] = DataDefinition::create('string')
      ->setLabel(t('Parent'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $schema = parent::schema($field_definition);
    // Add a unique ID to allow referencing the parent link.
    $schema['columns']['id'] = [
      'description' => 'Unique machine name.',
      'type' => 'varchar',
      'length' => 255,
    ];
    // Add a field for the parent link.
    $schema['columns']['parent'] = [
      'description' => 'The parent of the link.',
      'type' => 'varchar',
      'length' => 255,
    ];
    return $schema;
  }

}
