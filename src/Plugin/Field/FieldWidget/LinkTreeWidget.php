<?php

namespace Drupal\link_tree\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'link tree' widget.
 *
 * @FieldWidget(
 *   id = "link_tree_default",
 *   label = @Translation("Link tree"),
 *   field_types = {
 *     "link_tree"
 *   }
 * )
 */
class LinkTreeWidget extends LinkWidget {

  /**
   * The uuid service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setUuid($container->get('uuid'));
    return $instance;
  }

  /**
   * Sets uuid service.
   */
  public function setUuid(UuidInterface $uuid): void {
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $defaults = parent::defaultSettings();
    $defaults += [
      'max_depth' => 0,
    ];
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);
    $elements['max_depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Depth'),
      '#options' => $this->getLinkTreeDepthOptions(),
      '#default_value' => $this->getSetting('max_depth'),
      '#weight' => -3,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $options = $this->getLinkTreeDepthOptions();
    $summary[] = $this->t('Depth: @depth', ['@depth' => $options[$this->getSetting('max_depth')]]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    // Add a field for the unique ID.
    $element['id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('ID'),
      '#default_value' => $items[$delta]->id ?? 'link_tree_item:' . $this->uuid->generate(),
    ];
    // Add the ID for the parent link.
    $element['parent'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Parent'),
      '#default_value' => $items[$delta]->parent ?? NULL,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state): array {
    $elements = parent::formMultipleElements($items, $form, $form_state);
    // Pass the field settings.
    $elements['#settings'] = $this->getSettings();
    // Get the link tree.
    $elements['#link_tree'] = $items->getTree();
    // Flatten the link tree for easy depth lookups while creating the table.
    $elements['#link_tree_flat'] = $this->flattenTree($elements['#link_tree']);

    // Use custom theme function that support link trees.
    $elements['#theme'] = 'field_multiple_value_link_tree_form';
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state): void {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $form_state_values = $form_state->getValues();
    $values = NestedArray::getValue($form_state_values, $path, $key_exists);

    if ($key_exists) {
      // Account for drag-and-drop reordering if needed.
      if (!$this->handlesMultipleValues()) {
        // Remove the 'value' of the 'add more' button.
        unset($values['add_more']);

        // The original delta, before drag-and-drop reordering, is needed to
        // route errors to the correct form element.
        foreach ($values as $delta => &$value) {
          $value['_original_delta'] = $delta;
        }

        // Rearrange values to mach the link tree structure.
        $values = $this->flattenValuesTree($this->buildValuesTree($values));
      }

      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values and remove the empty ones.
      $items->setValue($values);
      $items->filterEmptyItems();

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      foreach ($items as $delta => $item) {
        $field_state['original_deltas'][$delta] = $item->_original_delta ?? $delta;
        unset($item->_original_delta, $item->_weight);
      }
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }
  }

  /**
   * Get the link tree depth options.
   *
   * @return array
   *   The link tree depth options.
   */
  protected function getLinkTreeDepthOptions(): array {
    return [
      0 => $this->t('Unlimited'),
      1 => $this->t('Limited to 1 child level'),
      2 => $this->t('Limited to 2 child levels'),
      3 => $this->t('Limited to 3 child levels'),
      4 => $this->t('Limited to 4 child levels'),
      5 => $this->t('Limited to 5 child levels'),
    ];
  }

  /**
   * Flatten the link tree and set a depth value per item.
   *
   * @param array $items
   *   The link items.
   * @param array $link_tree_flat
   *   The link items in a flattened array.
   * @param int $depth
   *   The depth of the link tree item.
   *
   * @return array
   *   The link tree as a flattened array.
   */
  protected function flattenTree(array $items, array &$link_tree_flat = [], int $depth = 0): array {
    foreach ($items as $id => $item) {
      $item->set('depth', $depth);
      $link_tree_flat[$id] = $item;
      if ($item->children) {
        $this->flattenTree($item->children, $link_tree_flat, $depth + 1);
      }
    }
    return $link_tree_flat;
  }

  /**
   * Build the link tree based on the link tree form values.
   *
   * @param array $values
   *   The link tree form values.
   * @param string $parent_id
   *   The parent ID of one or more link items.
   *
   * @return array
   *   The link tree.
   */
  protected function buildValuesTree(array &$values, string $parent_id = ''): array {
    $tree = [];

    foreach ($values as $delta => $value) {
      // Remove any empty link field.
      if (!$value['uri']) {
        unset($values[$delta]);
        continue;
      }

      // We definitely need an ID to build the link tree.
      if (!$id = $value['id']) {
        continue;
      }

      // Build the link tree.
      if ($value['parent'] === $parent_id) {
        $children = $this->buildValuesTree($values, $id);
        if ($children) {
          $value['has_children'] = TRUE;
          $value['children'] = $children;
        }
        $tree[$value['_weight']] = $value;
        unset($values[$delta]);
      }
    }

    ksort($tree);
    return $tree;
  }

  /**
   * Flatten the values tree and set a weight value per item.
   *
   * @param array $values
   *   The link tree values.
   * @param array $values_tree_flat
   *   The link items in a flattened array.
   * @param int $delta
   *   The depth of the link tree item.
   *
   * @return array
   *   The link tree as a flattened array.
   */
  protected function flattenValuesTree(array $values, array &$values_tree_flat = [], int &$delta = 0): array {
    foreach ($values as $value) {
      $value['_weight'] = $delta;
      $values_tree_flat[$delta] = $value;
      ++$delta;
      if (isset($value['children'])) {
        $this->flattenValuesTree($value['children'], $values_tree_flat, $delta);
      }
    }
    return $values_tree_flat;
  }

}
