# Link Tree
This module is an extension for the core Link module. It provides a simple
link field type in a tree structure.

On a Link field with multiple items the sequence can be altered.
On a Link Tree field with multiple items the sequence can be altered and
they can be indented.
